"""
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
"""
import os
import boto3
import numpy as np
from DataManipulator import DataNormalize


class Processing:

    def __init__(self):
        self.TRAINING_BUCKET = "training-data-mital-app"
        self.SERVING_BUCKET = "serving-data-mital-app"
        self.TRAIN = "train"
        self.MODELS_CONFIG = "models.config"

        self.dn = DataNormalize()

    def download_all_logs(self, src):
        print("모든 사물의 학습 데이터 다운로드 작업 시작")
        print("학습 데이터 버킷에 접속 중...")
        bucket = boto3.resource("s3").Bucket(self.TRAINING_BUCKET)

        print("모든 사물의 학습 데이터 다운로드 중...")
        for obj in bucket.objects.all():
            if self.TRAIN in obj.key:
                obj_path = obj.key[6:]
                if len(obj_path) != 0:
                    obj_path = obj_path.split("/")
                    thing_name = obj_path[0]
                    logfile_name = obj_path[1]
                    folder = os.path.join(src, thing_name)
                    os.makedirs(folder, exist_ok=True)
                    file = os.path.join(src, thing_name, logfile_name)
                    if not os.path.exists(file):
                        bucket.download_file(obj.key, file)
        print("모든 사물의 학습 데이터 다운로드 완료")

    def not_mv_avg_all(self, src, dst):
        print("모든 학습 데이터 조정 작업 시작")
        print("작업 중...")
        root_path = src
        dir_list = os.listdir(root_path)
        for thing_name in dir_list:
            file_list = os.listdir(os.path.join(root_path, thing_name))
            pre_folder = os.path.join(dst, thing_name)
            os.makedirs(pre_folder, exist_ok=True)
            for file in file_list:
                filepath = os.path.join(root_path, thing_name, file)
                prepared_filepath = os.path.join(dst, thing_name, file)
                if not os.path.exists(prepared_filepath):
                    print("조정 작업 중...", filepath)
                    self._save_processed_data(filepath, prepared_filepath, False)
        print("모든 학습 데이터 조정 작업 완료")

    def mv_avg_all(self, src, dst):
        print("모든 학습 데이터 이동 평균 작업 시작")
        print("이동 평균 작업 중...")
        root_path = src
        dir_list = os.listdir(root_path)
        for thing_name in dir_list:
            file_list = os.listdir(os.path.join(root_path, thing_name))
            pre_folder = os.path.join(dst, thing_name)
            os.makedirs(pre_folder, exist_ok=True)
            for file in file_list:
                filepath = os.path.join(root_path, thing_name, file)
                prepared_filepath = os.path.join(dst, thing_name, file)
                if not os.path.exists(prepared_filepath):
                    print("이동 평균 작업 중...", filepath)
                    self._save_processed_data(filepath, prepared_filepath)
        print("모든 학습 데이터 이동 평균 작업 완료")

    def _save_processed_data(self, path, save_path, mv_avg=True):
        dn = self.dn
        data = dn.get_data(path)
        new_data = data[:, -1] + 32400000  # 아마 한국시간대
        new_data = new_data[:] / 1000
        new_data = new_data[:] % 86400
        new_data1 = np.round(new_data[:] / 20)
        new_data1 = np.array(new_data1, dtype=np.int32)
        new_data1 = new_data1.reshape([np.shape(new_data1)[0], 1])
        new_data1 = dn.min_max_scaler(new_data1, _min=[0.], _max=[4320.])
        new_data1 = new_data1 * 100
        result = np.hstack([data[:, :-1], new_data1])
        if mv_avg:
            result = dn.moving_average(result, 15, 3)
        dn.saveCSV(save_path, result)

    def upload_all_models(self, src):
        print("모든 모델 업로드 작업 시작")
        print("모델 버킷에 접속 중...")
        bucket = boto3.resource("s3").Bucket(self.SERVING_BUCKET)

        print("모델 검색 중...")
        file_list = self._recursive_search(src, except_string="checkpoint")
        print("확인 된 모델 업로드 중...")
        for file in file_list:
            key = file.replace("\\", "/")
            key = key.replace("./models/", "")
            bucket.upload_file(file, key)
        print("모든 모델 업로드 완료")

    def _recursive_search(self, path, except_string=None, paths=None):
        if paths is None:
            paths = []
        if not os.path.isdir(path):
            paths.append(path)
            return
        for p in os.listdir(path):
            if except_string is not None and except_string in p:
                continue
            self._recursive_search(os.path.join(path, p), except_string=except_string, paths=paths)
        return paths

    def download_all_models(self, dst):
        print("모델 다운로드 작업 시작")
        print("모델 버킷에 접속 중...")
        bucket = boto3.resource("s3").Bucket(self.SERVING_BUCKET)
        result = set()
        print("모든 모델 다운로드 중...")
        for obj in bucket.objects.all():
            key = obj.key.split("/")
            folder = os.path.join(dst, *key[:-1])
            os.makedirs(folder, exist_ok=True)
            file = key[-1]
            if not os.path.exists(os.path.join(folder, file)):
                bucket.download_file(obj.key, os.path.join(folder, file))
            result.add(key[0])
        print("모든 모델 다운로드 완료")
        return result

    def generate_models_config(self, src, thing_names):
        print("models.config 생성 작업 시작")
        print("생성 중...")
        src = os.path.join(src, self.MODELS_CONFIG)
        base_path = "/models/"
        f = open(src, "w")
        f.write("model_config_list: {\n")
        for thing_name in thing_names:
            f.write("   config: {\n" +
                    "       name: \"" + thing_name + "\",\n" +
                    "       base_path: \"" + base_path + thing_name + "\",\n" +
                    "       model_platform: \"tensorflow\"\n" +
                    "   },\n")
        f.write("}")
        print("models.config 생성 완료")
        return {"base_path": base_path, "config_path": src, "config_name": self.MODELS_CONFIG}
