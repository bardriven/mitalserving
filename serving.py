"""
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
"""
# 서빙시, 시간 00:00 에 s3 모델 버킷에서 모델을 받아, 해당 리스트로 models.config 파일을 생성, 모델과 함께 인자를 주어 도커에서 서빙 실행
import os

from Prepare import Processing
from serving_in_docker import run_serving_in_docker

# 서빙 작업 시작
processing = Processing()
models_src = os.path.join(os.curdir, "models_bucket")

thing_names = processing.download_all_models(models_src)
paths = processing.generate_models_config(models_src, thing_names)

run_serving_in_docker(paths)