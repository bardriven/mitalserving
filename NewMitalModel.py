"""
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
"""
import os
import numpy as np
import tensorflow as tf


class MitalModel:
    def __init__(self, src, dst, tmp):
        checkpoint = os.path.join(dst, "checkpoint")
        os.makedirs(dst, exist_ok=True)
        os.makedirs(checkpoint, exist_ok=True)
        self.tmp = tmp
        tf.reset_default_graph()

        with tf.variable_scope("const"):
            self.seq_length = 50
            self.predict_length = 1
            self.window_size = self.seq_length + self.predict_length

            self.data_dim = 6
            self.output_dim = 2
            self.num_units = 224
            self.lstm1_num_units_arr = [224, 224, 224]

            self.learning_rate = 0.001
            self.keep_prob = 1 - 1e-10
            self.epoch = 50

            # batch_total_size = 0 # 0 - 227, 1 - 4331, 2 - 4331, 3 - 4321, 4 - 3947, 5 - 4332, 6 - 3282
            self.batch_size = 500

            self.checkpoint_path = checkpoint
            self.model_path = dst
            self.path = src

        with tf.variable_scope("batch_process"):
            self.lstm1_x, self.y, self.batch_total_size = self.get_batch_xy(self.path)

        with tf.variable_scope("placeholder"):
            self.lstm1_X = tf.placeholder(tf.float32, [None, self.seq_length, self.data_dim], name="lstm1_X")
            self.Y = tf.placeholder(tf.float32, [None, self.predict_length, self.output_dim], name="Y")  # 여러값 리턴을
            #  위해 차원 추가
            self.Keep_prob = tf.placeholder(tf.float32, name="Keep_prob")
            self.epoch_tf = tf.Variable(0, name="epoch_tf", dtype=tf.int32)
            self.version_tf = tf.Variable(0, name="version_tf", dtype=tf.int32)

        with tf.variable_scope("lstm1"):
            lstms1 = []
            for hidden_dim in self.lstm1_num_units_arr:
                lstm1 = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True, activation=tf.nn.tanh)
                lstms1.append(tf.contrib.rnn.DropoutWrapper(lstm1, output_keep_prob=self.Keep_prob))
            cells = tf.contrib.rnn.MultiRNNCell(lstms1, state_is_tuple=True)
            lstm1_outputs, lstm1_state = tf.nn.dynamic_rnn(cell=cells, inputs=self.lstm1_X, dtype=tf.float32)

        with tf.variable_scope("prediction"):
            dense1_output = tf.layers.dense(lstm1_outputs[:, self.seq_length - 1:], self.num_units,
                                            activation=tf.nn.relu)
            # dense2_output = tf.layers.dense(dense1_output, num_units, activation=tf.nn.relu)
            dense1_dropout = tf.layers.dropout(dense1_output, rate=self.Keep_prob)
            # self.predict = tf.layers.dense(dropout[:, self.seq_length - 1:], self.output_dim,
            #                                activation=None, name="predict")
            self.predict = tf.layers.dense(dense1_dropout, self.output_dim,
                                           activation=None, name="predict")
            # self.predict = tf.contrib.layers.fully_connected(lstm1_outputs[:, -1], self.output_dim, activation_fn=None)
            # predict = tf.identity(lstm1_outputs[:, seq_length:], name="predict")

        with tf.variable_scope("train"):
            # loss = tf.reduce_mean(tf.square(Y - predict))
            self.loss = tf.reduce_mean(tf.squared_difference(self.Y, self.predict), name="loss")
            # self.loss = tf.reduce_sum(tf.square(self.predict - self.Y), name="loss")
            optimizer = tf.train.AdamOptimizer(self.learning_rate)
            self.train = optimizer.minimize(self.loss, name="train")

    def run(self):
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            saver = tf.train.Saver()

            try:
                saver.restore(sess, tf.train.latest_checkpoint(self.checkpoint_path))
                # graph = tf.get_default_graph()
            except ValueError:
                print("First epoch.")

            for_version = 0
            start_epoch = sess.run(self.epoch_tf)
            for step in range(start_epoch, self.epoch + start_epoch):
                _loss = 0
                batch_loop_length = int(self.batch_total_size / self.batch_size)
                if self.batch_total_size % self.batch_size > 0:
                    batch_loop_length += 1

                for batch_step in range(batch_loop_length):
                    train_x, train_y = sess.run([self.lstm1_x, self.y])
                    _, l = sess.run([self.train, self.loss],
                                    feed_dict={self.lstm1_X: train_x, self.Y: train_y, self.Keep_prob: self.keep_prob})
                    _loss += l / batch_loop_length
                    # print(step + 1, "/", batch_loop_length, "/", batch_step + 1, "/", l)

                if (step + 1) % 5 == 0:
                    sess.run(self.epoch_tf.assign(step + 1))
                    saver.save(sess, os.path.join(self.checkpoint_path, "checkpoint"), global_step=(step + 1))
                    for_version = step + 1
                print("epoch: ", step + 1, "loss: ", _loss)

            ver = sess.run(self.version_tf)
            version = str(ver)
            sess.run(self.version_tf.assign(int(ver) + 1))
            saver.save(sess, os.path.join(self.checkpoint_path, "checkpoint"), global_step=for_version)
            self.save_model(sess, version)

    def get_batch_xy(self, root_path):
        total_line = 0

        paths = [os.path.join(root_path, i) for i in os.listdir(root_path)]

        start_file_index = len(paths) - self.tmp  # 25
        end_file_index = len(paths) - self.tmp - 1
        print("start_file_index: ", self.tmp, ", end_file_index: ", self.tmp - 1)
        x = np.loadtxt(paths[start_file_index], delimiter=",")
        print("file number", start_file_index)
        total_line += x.shape[0]
        data_set = tf.data.Dataset.from_tensor_slices(x)
        for i in range(start_file_index + 1, end_file_index):
            x = np.loadtxt(paths[i], delimiter=",")
            print("file number", i)
            total_line += x.shape[0]
            data_set_tmp = tf.data.Dataset.from_tensor_slices(x)
            data_set = data_set.concatenate(data_set_tmp)

        data_set = data_set.apply(tf.contrib.data.sliding_window_batch(window_size=self.window_size, stride=1))

        data_set = data_set.repeat().shuffle(total_line).batch(self.batch_size)
        iterator = data_set.make_one_shot_iterator()
        next_element = iterator.get_next()

        x = next_element[:, :self.seq_length]
        # pad = tf.constant([[0, 0], [0, self.predict_length], [0, 0]])

        # padded_x = tf.pad(non_pad_x, pad, "CONSTANT")
        label = next_element[:, -1:, 2:4]
        return x, label, total_line - self.window_size

    def save_model(self, sess, version):
        prediction_inputs = tf.saved_model.utils.build_tensor_info(
            self.lstm1_X)
        prediction_inputs_keep_prob = tf.saved_model.utils.build_tensor_info(
            self.Keep_prob)
        prediction_outputs = tf.saved_model.utils.build_tensor_info(
            self.predict)

        prediction_signature = (
            tf.saved_model.signature_def_utils.build_signature_def(
                inputs={
                    # tf.saved_model.signature_constants.PREDICT_INPUTS:
                    "lstm1_X":
                        prediction_inputs,
                    "Keep_prob": prediction_inputs_keep_prob
                },
                outputs={
                    tf.saved_model.signature_constants.PREDICT_OUTPUTS:
                        prediction_outputs
                },
                method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))
        builder = tf.saved_model.builder.SavedModelBuilder(os.path.join(self.model_path, version))  # version
        builder.add_meta_graph_and_variables(sess, [tf.saved_model.tag_constants.SERVING], signature_def_map={
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
                prediction_signature,
        }, strip_default_attrs=True)
        builder.save()
