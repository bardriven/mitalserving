# MitalServing
Tensorflow API로 만든 LSTM 모델에 미세먼지, 초미세먼지, 온도, 습도, 시간 데이터를 학습하여 다음 순간의 미세먼지 농도를 예측.

training.py 를 이용해 누적 학습 후, 해당 모델을
serving.py 에서 Tensorflow Serving with Dorker 로 REST API로 서비스.

모델은 BasicLSTMCell로 구성되며 3층 레이어, 학습 능력을 결정하는 num_units = 192, 학습 속도(learning_rate)는 0.001 로 설정.
N:1 모델로 마지막 출력은 완전 연결 층(fullyConnectedLayer) 이용함.