"""
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
"""
# 학습시, 시간 00:00 에 s3 데이터 버킷에서 데이터를 받아 해당 데이터로 학습후, s3 모델 버킷에 저장
import os

from NewMitalModel2 import MitalModel
from Prepare import Processing

prepare_src = os.path.join(os.curdir, "train_bucket", "train")
train_src = prepare_dst = os.path.join(os.curdir, "prepared")
train_dst = os.path.join(os.curdir, "models")

processing = Processing()
processing.download_all_logs(prepare_src)
# processing.mv_avg_all(prepare_src, prepare_dst)
processing.not_mv_avg_all(prepare_src, prepare_dst)
# i = 11  # 33
# 병렬로 수행해야하는 작업이지만.. 그냥함
# while i >= 1:
for thing_name in os.listdir(train_src):
    for pm in ("pm25", "pm10"):
        # if thing_name == "latte_2": continue
        # mitalModel = MitalModel(os.path.join(train_src, thing_name), os.path.join(train_dst, thing_name))
        mitalModel = MitalModel(os.path.join(train_src, thing_name), os.path.join(train_dst, thing_name), pm, i)
        print(thing_name + "_" + pm, "기기 학습 시작")
        mitalModel.run()
        print(thing_name + "_" + pm, "기기 학습 종료")
        del mitalModel
processing.upload_all_models(train_dst)
    # i -= 1

# processing.upload_all_models(train_dst)
