"""
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
"""
import os
import subprocess
import sys
import signal


def signal_term_handler(sig, frame):
    print("종료 신호: ", signal.Signals(sig).name)
    stop_rm_serving()
    sys.exit(0)


def stop_rm_serving():
    print("컨테이너 종료 후 제거 시도")
    stop_comm = "docker stop serving -t 1 && docker rm -v serving"
    stop_process = subprocess.Popen(stop_comm,
                                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
    for line in stop_process.stdout:
        sys.stdout.write("stop_p_stdout: " + line)
    for line in stop_process.stderr:
        sys.stderr.write("stop_p_stderr: " + line)
    print("컨테이너 종료 후 제거 성공")


def run_serving_in_docker(paths):
    stop_rm_serving()
    print("종료 이벤트 등록 중...")
    signal.signal(signal.SIGTERM, signal_term_handler)
    signal.signal(signal.SIGINT, signal_term_handler)

    print("tensorflow/serving 컨테이너 실행")
    models_src = os.path.dirname(paths["config_path"])
    # models_src = os.path.join(os.curdir, "models_bucket")
    # docker run --name serving -p 8500:8500 -p 8501:8501 --mount type=bind,source=\"
    your_command = "docker run --name serving -p 8093:8501 --mount type=bind,source=\"" + \
                   os.path.abspath(models_src) + \
                   "\",target=" + paths["base_path"] + \
                   " -t tensorflow/serving:1.11.0-rc1 --model_config_file=" + paths["base_path"] + paths["config_name"]
    # your_command = "docker restart serving"
    # print(your_command)
    serving_process = subprocess.Popen(your_command,
                                       stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    for line in serving_process.stdout:
        sys.stdout.write("serving_p_stdout: " + line)
    for line in serving_process.stderr:
        sys.stderr.write("serving_p_stderr: " + line)
